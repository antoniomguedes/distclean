
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JOptionPane;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class MenuController {

    @FXML
    private Button bdc;

    @FXML
    private Button pdv;

    @FXML
    private Button hdp;

    @FXML
    private Button v;

    @FXML
    private Button ldp;

    @FXML
    private Button rp;

    @FXML
    private Button rdp;

    @FXML
    private Button bdp;

    @FXML
    void pedidosVips(ActionEvent event) {

    }

    @FXML
    void histPedidos(ActionEvent event) {

    }

    @FXML
    void vendas(ActionEvent event) {
    	try {
    	ArrayList<Produto> produtos = ProdutoBODAO.listProdutos();
    	ArrayList<String> nomesProdutos = new ArrayList<>();
    	int codProduto=0;
    	for (Produto p : produtos) {
    		nomesProdutos.add(p.getNome());
    	}
    	String produto = (String)JOptionPane.showInputDialog(null, "Selecione o produto para informações de Vendas: " , "" ,
 				JOptionPane.PLAIN_MESSAGE , null ,nomesProdutos.toArray(),"");
    	for (Produto p : produtos) {
    		if (p.getNome().equals(produto))
    			codProduto=p.getCodProduto();
    	}
    	
    	String infvendas = PedidoBODAO.vendas(codProduto);
    	String infdesc = PedidoBODAO.vendasDesconto(codProduto);
    	StringBuilder mens = new StringBuilder();
    	mens.append(infvendas);
    	mens.append(" ");
    	mens.append(infdesc);
    	mens.append("\n");
    	JOptionPane.showMessageDialog(null,mens, "Relatório de Pedidos", 2);
    	
    	
    	}
    	catch (Exception e) {
    		System.out.println("Erro nas vendas");
    	}
    }

    @FXML
    void relatorioPedidos(ActionEvent event) {
    	try {
    		ArrayList<String> relatorio = PedidoBODAO.relatorio();
    		StringBuilder exibe = new StringBuilder();
    		for (String s : relatorio) {
    			exibe.append(s + "\n");
    		}
    		JOptionPane.showMessageDialog(null,exibe, "Relatório de Pedidos", 2);
    		
    	}
    	catch (Exception e) {
    		System.out.println("Erro para realizar o relatório");
    	}

    }

    @FXML
    void realizaPedido(ActionEvent event) {
    	try {
    	int codCliente =0;
    	int codProduto=0;
    	ArrayList<Cliente> clientes = ClienteBODAO.listClientes();
    	ArrayList<Produto> produtos = ProdutoBODAO.listProdutos();
    	ArrayList<String> nomeClientes = new ArrayList<>();
    	ArrayList<String> nomeProdutos = new ArrayList<>();
    	for (Cliente c : clientes) {
    		nomeClientes.add(c.getNome());
    	}
    	for (Produto p : produtos) {
    		nomeProdutos.add(p.getNome());
    	}
    	String cliente = (String)JOptionPane.showInputDialog(null, "Selecione o cliente que esta fazendo o pedido: " , "" ,
 				JOptionPane.PLAIN_MESSAGE , null ,nomeClientes.toArray(),"");
    	for (Cliente c : clientes) {
    		if (c.getNome().equals(cliente))
    			codCliente = c.getCodCliente();
    	}
    	String produto = (String)JOptionPane.showInputDialog(null, "Selecione o produto do pedido: " , "" ,
 				JOptionPane.PLAIN_MESSAGE , null ,nomeProdutos.toArray(),"");
    	for (Produto p : produtos) {
    		if (p.getNome().equals(produto))
    			codProduto=p.getCodProduto();
    	}
    	String quantidade = JOptionPane.showInputDialog("Por favor, informe a quantidade do produto ");
    	int quant = Integer.parseInt(quantidade);
    	String valorTotal = JOptionPane.showInputDialog("Por favor, informe o valor total da compra ");
    	double vt = Double.parseDouble(valorTotal);
    	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    	Date hoje= new Date();
    	String data = df.format(hoje);
    	//System.out.println(data);
    	
    	
    	
    	PedidoBODAO.realizarPedido(codCliente, codProduto, quant, vt, data);
    		
    	
    	}
    	catch (Exception e) {
    		System.out.println("Problemas para realizar pedido!");
    	}
    }

    @FXML
    void buscaProdutos(ActionEvent event) {
    	ArrayList<String> opcoes = new ArrayList<>();
    	opcoes.add("Nome");
    	opcoes.add("Categoria");
    	String res = (String)JOptionPane.showInputDialog(null, "Selecione o parâmetro de busca: " , "" ,
 				JOptionPane.PLAIN_MESSAGE , null ,opcoes.toArray(),"");
    	if (res.equals("Nome")) {
    		String nomeProduto = JOptionPane.showInputDialog("Por favor, digite o nome do produto: ");
        	try {
        		StringBuilder exibe = new StringBuilder();
        		ArrayList<Produto> produtos = ProdutoBODAO.buscaProdutoNome(nomeProduto);
        		for (Produto p : produtos) {
        			exibe.append("Nome: "+p.getNome()+ " Categoria: "+p.getCategoria()+ " Preco: R$"+ p.getPreco()+"\n");
        		}
        		JOptionPane.showMessageDialog(null,exibe, "Busca de produtos por nome", 2);
        	}
        	catch (Exception e) {
        		System.out.println("Falha para os produtos por nome!");
        	}
    	}
    	else {
    		String desProduto = JOptionPane.showInputDialog("Por favor, digite a categoria do produto: ");
    		try {
        		StringBuilder exibe = new StringBuilder();
        		ArrayList<Produto> produtos = ProdutoBODAO.buscaProdutoDescricao(desProduto);
        		for (Produto p : produtos) {
        			exibe.append("Nome: "+p.getNome()+ " Categoria: "+p.getCategoria()+ " Preco: R$"+ p.getPreco()+"\n");
        		}
        		JOptionPane.showMessageDialog(null,exibe, "Busca de produtos por categoria", 2);
        	}
        	catch (Exception e) {
        		System.out.println("Falha para listar os produtos por descrição!");
        	}
    	}
    		
    		

    }

    @FXML
    void buscaCliente(ActionEvent event) {
    	String nome = JOptionPane.showInputDialog("Por favor, digite o nome do cliente: ");
    	try {
    		StringBuilder exibe = new StringBuilder();
    		ArrayList<Cliente> clientes = ClienteBODAO.buscaCliente(nome);
    		for (Cliente c : clientes) {
    			exibe.append("Nome: "+c.getNome()+ " Endereço: "+c.getEndereco()+ " Telefone: "+ c.getTelefone()+ " Status: "+ c.getStatus()+"\n");
    		}
    		JOptionPane.showMessageDialog(null,exibe, "Busca de Clientes", 2);
    	}
    	catch (Exception e) {
    		System.out.println("Falha para listar os clientes!");
    	}
    }

    @FXML
    void listProd(ActionEvent event) {
    	try {
    		StringBuilder exibe = new StringBuilder();
    		ArrayList<Produto> produtos = ProdutoBODAO.listProdutos();
    		for (Produto p : produtos) {
    			exibe.append("Nome: "+p.getNome()+ " Categoria: "+p.getCategoria()+ " Preco: R$"+ p.getPreco()+"\n");
    		}
    		JOptionPane.showMessageDialog(null,exibe, "Lista de Produtos", 2);
    	}
    	catch (Exception e) {
    		System.out.println("Falha para listar os produtos!");
    	}
    }
}
