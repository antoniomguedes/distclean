

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Main extends Application {
	  private Stage primaryStage;
	  private BorderPane rootLayout;

	 public static void main(String[] args) {
	  launch(); 
	 }

	 @Override
	 public void start(Stage stage) throws Exception {
		    Parent root = FXMLLoader.load(getClass().getResource("sd.fxml"));
		    
	        Scene scene = new Scene(root, 258, 400);
	    
	        stage.setTitle("DistClean");
	        stage.setScene(scene);
	        stage.show();
	        
	    }
	/* public void start(Stage stage) throws Exception {
	 FXMLLoader loader = new FXMLLoadergetClass().getResource("main.fxml");
	 loader.setController(new MainController(path));
	 Pane mainPane = loader.load();
	 }*/
}