
public class Cliente {
	private int codCliente;
	private String nome;
	private String endereco;
	private String telefone;
	private String status;

	public Cliente(int codCliente, String nome, String endereco, String telefone, String status) {
		this.codCliente=codCliente;
		this.nome=nome;
		this.endereco=endereco;
		this.telefone=telefone;
		this.status=status;
	}

	public int getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(int codCliente) {
		this.codCliente = codCliente;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
