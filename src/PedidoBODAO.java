import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

public class PedidoBODAO {
	public static void realizarPedido(int codCliente, int codProduto, int quantidade, double ValorTotal, String data) throws SQLException {
		Connection conexao = DriverManager.getConnection(Conexao.endereco, Conexao.user, Conexao.password);
		String sql = "INSERT INTO Pedido (DataCriacao, Quantidade, ValorTotal, CodProduto, CodCliente) VALUES ('"+data+"', "+quantidade+", "+ValorTotal+", "+codProduto+", "+codCliente+")";
		PreparedStatement stmt = conexao.prepareStatement(sql);
		stmt.execute();
		conexao.close();
		//return true;
	}
	
	public static ArrayList<String> relatorio() throws SQLException {
		ArrayList<String> resultado = new ArrayList<>();
		Connection conexao = DriverManager.getConnection(Conexao.endereco, Conexao.user, Conexao.password);
		String sql = "SELECT C.NOME NOMECLIENTE, PROD.NOME NOMEPRODUTO, PED.DATACRIACAO, PED.VALORTOTAL "
				+ "FROM CLIENTE C, PRODUTO PROD, PEDIDO PED "
				+ "WHERE C.CODCLIENTE=PED.CODCLIENTE AND PROD.CODPRODUTO=PED.CODPRODUTO";
		PreparedStatement stmt = conexao.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			String nomeCliente = rs.getString("NOMECLIENTE");
			String nomeProduto = rs.getString("NOMEPRODUTO");
			Date dataCriacao = rs.getDate("DATACRIACAO");
			double preco = rs.getDouble("VALORTOTAL");
			String exemplo = ("Cliente: "+ nomeCliente+" Nome produto: "+ nomeProduto+ " Data do pedido: "+ dataCriacao+ " Valor total: R$"+ preco);
			resultado.add(exemplo);
		}
		rs.close();
		stmt.close();
		conexao.close();
		return resultado;
	}
	
	public static String vendas(int codProd) throws SQLException {
		Connection conexao = DriverManager.getConnection(Conexao.endereco, Conexao.user, Conexao.password);
		String sql = "SELECT COUNT(*) QTDPEDIDO, SUM(PED.QUANTIDADE) TOTALPRODUTOS, SUM(PED.VALORTOTAL) TOTALVENDIDADO FROM PRODUTO PROD, PEDIDO PED WHERE PROD.CODPRODUTO=" +codProd+ " AND PED.CODPRODUTO=PROD.CODPRODUTO";
		PreparedStatement stmt = conexao.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		String exemplo = null;
		while (rs.next()) {
			int qtdPedido = rs.getInt("QTDPEDIDO");
			int totalProdutos = rs.getInt("TOTALPRODUTOS");
			double totalVendidado = rs.getDouble("TOTALVENDIDADO");
			//double preco = rs.getDouble("VALORTOTAL");
			exemplo = ("Quantidade de pedidos: "+ qtdPedido+" Total de Produtos: "+ totalProdutos+ " Total Vendido: "+ totalVendidado);
		}
		rs.close();
		stmt.close();
		conexao.close();
		return exemplo;
	}
	
	public static String vendasDesconto(int codProd) throws SQLException {
		Connection conexao = DriverManager.getConnection(Conexao.endereco, Conexao.user, Conexao.password);
		String sql = "SELECT SUM(PED.VALORTOTAL)*0.1 TOTALDESCONTO FROM CLIENTE C, PEDIDO PED WHERE PED.CODPRODUTO="+codProd+"C.STATUS='Vip' AND PED.CODCLIENTE=C.CODCLIENTE;";
		PreparedStatement stmt = conexao.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		String exemplo = null;
		while (rs.next()) {
			double totalDesconto = rs.getDouble("TOTALDESCONTO");
			//double preco = rs.getDouble("VALORTOTAL");
			exemplo = ("Total de vendas com desconto: "+ totalDesconto);
		}
		rs.close();
		stmt.close();
		conexao.close();
		return exemplo;
	}

}
