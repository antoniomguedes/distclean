
public class Pedido {
	private int numero;
	private String dataCriacao;
	private int quantidade;
	private int valorTotal;
	private int codProduto;
	private int codCliente;
	
	public Pedido(int numero, String dataCriacao, int quantidade, int valorTotal, int codProduto, int codCliente) {
		this.numero=numero;
		this.dataCriacao=dataCriacao;
		this.quantidade=quantidade;
		this.valorTotal=valorTotal;
		this.codProduto=codProduto;
		this.codCliente=codCliente;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(String dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public int getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(int valorTotal) {
		this.valorTotal = valorTotal;
	}

	public int getCodProduto() {
		return codProduto;
	}

	public void setCodProduto(int codProduto) {
		this.codProduto = codProduto;
	}

	public int getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(int codCliente) {
		this.codCliente = codCliente;
	}
	
	

}
