import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ProdutoBODAO {

	public static ArrayList<Produto> listProdutos() throws SQLException {
		ArrayList<Produto> produtos = new ArrayList<>();
		Connection conexao = DriverManager.getConnection(Conexao.endereco, Conexao.user, Conexao.password);
		String sql = "SELECT CODPRODUTO, NOME, CATEGORIA , PRECO FROM PRODUTO ORDER BY NOME";
				PreparedStatement stmt = conexao.prepareStatement(sql);
				ResultSet rs = stmt.executeQuery();
				while (rs.next()) {
					Integer codProduto = rs.getInt("CODPRODUTO");
					String nome = rs.getString("NOME");
					String descricao = rs.getString("CATEGORIA");
					double preco = rs.getDouble("PRECO");
				 	produtos.add(new Produto(codProduto.intValue(), nome, descricao, preco));
				}
		rs.close();
		stmt.close();
		conexao.close();
		return produtos;
	}
	
	public static ArrayList<Produto> buscaProdutoNome(String nomeProduto) throws SQLException {
		ArrayList<Produto> produtos = new ArrayList<>();
		Connection conexao = DriverManager.getConnection(Conexao.endereco, Conexao.user, Conexao.password);
		String sql = "SELECT CODPRODUTO, NOME, CATEGORIA , PRECO FROM PRODUTO WHERE NOME LIKE '%"+nomeProduto+"%' ORDER BY NOME";
		PreparedStatement stmt = conexao.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			Integer codProduto = rs.getInt("CODPRODUTO");
			String nome = rs.getString("NOME");
			String descricao = rs.getString("CATEGORIA");
			double preco = rs.getDouble("PRECO");
		 	produtos.add(new Produto(codProduto.intValue(), nome, descricao, preco));
		}
		rs.close();
		stmt.close();
		conexao.close();
		return produtos;
	}
	
	public static ArrayList<Produto> buscaProdutoDescricao(String desProduto) throws SQLException {
		ArrayList<Produto> produtos = new ArrayList<>();
		Connection conexao = DriverManager.getConnection(Conexao.endereco, Conexao.user, Conexao.password);
		String sql = "SELECT CODPRODUTO, NOME, CATEGORIA , PRECO FROM PRODUTO WHERE CATEGORIA LIKE '%"+desProduto+"%' ORDER BY NOME";
		PreparedStatement stmt = conexao.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			Integer codProduto = rs.getInt("CODPRODUTO");
			String nome = rs.getString("NOME");
			String descricao = rs.getString("CATEGORIA");
			double preco = rs.getDouble("PRECO");
		 	produtos.add(new Produto(codProduto.intValue(), nome, descricao, preco));
		}
		rs.close();
		stmt.close();
		conexao.close();
		return produtos;
	}
}
